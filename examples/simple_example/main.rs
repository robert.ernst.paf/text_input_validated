use iced::{Sandbox, Settings};
use text_input_validated::{text_input_validated, Validator};
use validations::{SimpleError, SimpleErrors, Validate};

fn main() {
    let _ = MainWindow::run(Settings::default());
}

#[derive(Debug, Clone)]
struct TextValidator {
    value: String,
}

impl Validator<String> for TextValidator {
    fn as_str(&self) -> &str {
        &self.value
    }
    fn value_str(&mut self, value_str: &str) {
        self.value = String::from(value_str);
    }

    fn to_value(&self) -> Option<String> {
        if self.validate().is_ok() {
            Some(self.value.clone())
        } else {
            None
        }
    }
}

impl From<String> for TextValidator {
    fn from(value: String) -> Self {
        TextValidator { value }
    }
}

impl Validate<()> for TextValidator {
    fn validate(&self) -> Result<(), validations::Errors<()>> {
        if self.value.len() % 2 == 1 {
            Ok(())
        } else {
            let mut errors = SimpleErrors::new();
            errors.add_error(SimpleError::new("must be odd length"));
            Err(errors)
        }
    }
}

#[derive(Debug, Clone)]
enum Message {
    ValueChanged(String),
}
struct MainWindow {
    value: TextValidator,
}

impl Sandbox for MainWindow {
    type Message = Message;

    fn new() -> Self {
        Self {
            value: TextValidator { value: "".into() },
        }
    }

    fn title(&self) -> String {
        "Example TextInputValidated".into()
    }

    fn update(&mut self, message: Self::Message) {
        match message {
            Message::ValueChanged(value) => self.value = value.into(),
        }
    }

    fn view(&self) -> iced::Element<Self::Message> {
        let text_input_validated =
            text_input_validated("placeholder", &self.value).on_valid_input(Message::ValueChanged);
        text_input_validated.into()
    }
}
