#![warn(missing_docs)]
//! A text input widget for iced providing a validation interface
//!
//! Icon by <a href='https://iconpacks.net/?utm_source=link-attribution&utm_content=10030'>Iconpacks</a>
//! Icon by <a href='https://iconpacks.net/?utm_source=link-attribution&utm_content=4794'>Iconpacks</a>
mod validator;

use iced::{
    widget::{
        component, row, svg,
        text::LineHeight,
        text_input,
        text_input::{Icon, Id},
        tooltip, Component,
    },
    Color, Element, Font, Length, Padding, Pixels, Renderer, Theme,
};
pub use validator::Validator;

/// A field that can be filled with text that will be validated.
///
/// # Example
///
/// ```
/// #[derive(Debug, Clone)]
/// enum Message {
///     TextInputChanged(ValidatedString)
/// }
///
/// let value = ValidatedString
pub struct TextInputValidated<'a, Message, V, T>
where
    V: Validator<T> + Clone,
{
    id: Option<Id>,
    placeholder: String,
    value: V,
    is_secure: bool,
    font: Option<Font>,
    width: Length,
    padding: Padding,
    size: Option<f32>,
    line_height: LineHeight,
    on_input: Option<Box<dyn Fn(String) -> Message + 'a>>,
    on_valid_input: Option<Box<dyn Fn(T) -> Message + 'a>>,
    on_paste: Option<Box<dyn Fn(String) -> Message + 'a>>,
    on_valid_paste: Option<Box<dyn Fn(T) -> Message + 'a>>,
    on_submit: Option<Message>,
    on_valid_submit: Option<Message>,
    icon: Option<Icon<Font>>,
    //    style: <Renderer::Theme as StyleSheet>::Style,
    valid_active_color: Color,
    valid_focused_color: Color,
    valid_hovered_color: Color,
    invalid_active_color: Color,
    invalid_focused_color: Color,
    invalid_hovered_color: Color,
    error_message: String,
    validator_image_size: f32,
}

/// The default [`Padding`] of a [`TextInputValidated`].
pub const DEFAULT_PADDING: Padding = Padding::new(5.0);

/// Creates a new [`TextInputValidated`].
///
/// [`TextInputValidated`]: crate::TextInputValidated
pub fn text_input_validated<'a, Message, V, T>(
    placeholder: &str,
    value: &V,
) -> TextInputValidated<'a, Message, V, T>
where
    V: Validator<T> + Clone,
    Message: Clone,
{
    TextInputValidated::new(placeholder, value)
}

/// Internal Events
#[derive(Debug, Clone)]
pub enum Event {
    /// Underlaying text input value changed
    OnChange(String),
    /// Underlaying text input value pasted
    OnPaste(String),
    /// Underlaying text input submitted
    OnSubmit,
}

/// Internal validation state
#[derive(Debug, Default, Clone, Copy, PartialEq)]
pub enum State {
    /// Validation is valid
    Valid,
    /// Validation is invalid
    Invalid,
    /// So far not checked
    #[default]
    Unchecked,
}

impl<'a, Message, V, T> TextInputValidated<'a, Message, V, T>
where
    Message: Clone,
    V: Validator<T> + Clone,
{
    /// Creates a new [`TextInputValidated`].
    ///
    /// It expects:
    /// - a placeholder,
    /// - the current value
    /// - the message which shall be send out on sumit
    pub fn new(placeholder: &str, value: &V) -> Self {
        Self {
            id: None,
            placeholder: String::from(placeholder),
            value: value.clone(),
            is_secure: false,
            font: None,
            width: Length::Fill,
            padding: DEFAULT_PADDING,
            size: None,
            line_height: LineHeight::default(),
            on_input: None,
            on_paste: None,
            on_submit: None,
            icon: None,
            valid_active_color: Color::from_rgb8(0, 255, 0),
            valid_focused_color: Color::from_rgb8(0, 255, 0),
            valid_hovered_color: Color::from_rgb8(0, 255, 0),
            invalid_active_color: Color::from_rgb8(255, 0, 0),
            invalid_focused_color: Color::from_rgb8(255, 0, 0),
            invalid_hovered_color: Color::from_rgb8(255, 0, 0),
            error_message: String::new(),
            validator_image_size: 30.0,
            on_valid_input: None,
            on_valid_paste: None,
            on_valid_submit: None,
        }
    }

    /// Sets the [`Id`] of the [`TextInputValidated`].
    pub fn id(mut self, id: Id) -> Self {
        self.id = Some(id);
        self
    }

    /// Converts the [`TextInputValidated`] into a secure password input.
    pub fn password(mut self) -> Self {
        self.is_secure = true;
        self
    }

    /// Sets the message that should be produced when some text is typed into
    /// the [`TextInputValidated`].
    /// The returned String is not validated
    ///
    /// If `on_valid_input` is also set and value is valid only `on_valid_input` will be produced
    /// and not `on_input`
    pub fn on_input<F>(mut self, callback: impl Fn(String) -> Message + 'a) -> Self {
        self.on_input = Some(Box::new(callback));
        self
    }

    /// Sets the message that should be produced when some text is pasted into
    /// the [`TextInputValidated`].
    ///
    /// If `on_valid_paste` is also set and value is valid only `on_valid_paste` will be produced
    /// and not `on_paste`
    pub fn on_paste(mut self, callback: impl Fn(String) -> Message + 'a) -> Self {
        self.on_paste = Some(Box::new(callback));
        self
    }

    /// Sets the message that should be produced when the [`TextInputValidated`] is
    /// focused and the enter key is pressed.
    ///
    /// If `on_valid_submit` is also set and value is valid only `on_valid_submit` will be produced
    /// and not `on_submit`
    pub fn on_submit(mut self, callback: Message) -> Self {
        self.on_submit = Some(callback);
        self
    }

    /// Sets the message that should be produced when some text is typed into
    /// the [`TextInputValidated`] and the text must be valid.
    ///
    /// If `on_valid_input` is also set and value is valid only `on_valid_input` will be produced
    /// and not `on_input`
    pub fn on_valid_input(mut self, callback: impl Fn(T) -> Message + 'a) -> Self {
        self.on_valid_input = Some(Box::new(callback));
        self
    }

    /// Sets the message that should be produced when some text is pasted into
    /// the [`TextInputValidated`].
    ///
    /// If `on_valid_paste` is also set and value is valid only `on_valid_paste` will be produced
    /// and not `on_paste`
    pub fn on_valid_paste(mut self, callback: impl Fn(T) -> Message + 'a) -> Self {
        self.on_valid_paste = Some(Box::new(callback));
        self
    }

    /// Sets the message that should be produced when the [`TextInputValidated`] is
    /// focused and the enter key is pressed.
    ///
    /// If `on_valid_submit` is also set and value is valid only `on_valid_submit` will be produced
    /// and not `on_submit`
    pub fn on_valid_submit(mut self, callback: Message) -> Self {
        self.on_valid_submit = Some(callback);
        self
    }

    /// Sets the [`Font`] of the [`TextInputValidated`].
    ///
    /// [`Font`]: iced::Font
    pub fn font(mut self, font: Font) -> Self {
        self.font = Some(font);
        self
    }

    /// Sets the [`Icon`] of the [`TextInputValidated`].
    pub fn icon(mut self, icon: Icon<Font>) -> Self {
        self.icon = Some(icon);
        self
    }

    /// Sets the width of the [`TextInputValidated`].
    pub fn width(mut self, width: impl Into<Length>) -> Self {
        self.width = width.into();
        self
    }

    /// Sets the [`Padding`] of the [`TextInputValidated`].
    pub fn padding<P: Into<Padding>>(mut self, padding: P) -> Self {
        self.padding = padding.into();
        self
    }

    /// Sets the text size of the [`TextInputValidated`].
    pub fn size(mut self, size: impl Into<Pixels>) -> Self {
        self.size = Some(size.into().0);
        self
    }

    /// Sets the [`LineHeight`] of the [`TextInputValidated`].
    pub fn line_height(mut self, line_height: impl Into<LineHeight>) -> Self {
        self.line_height = line_height.into();
        self
    }

    /// Set the validator image size of the [`TextInputValidated`].
    pub fn validator_image_size(mut self, image_size: f32) -> Self {
        self.validator_image_size = image_size;
        self
    }

    /// Set the border color in active case when input is valid
    pub fn valid_active_color(mut self, color: Color) -> Self {
        self.valid_active_color = color;
        self
    }

    /// Set the border color in active case when input is invalid
    pub fn invalid_active_color(mut self, color: Color) -> Self {
        self.invalid_active_color = color;
        self
    }

    /// Set the border color in focused case when input is valid
    pub fn valid_focused_color(mut self, color: Color) -> Self {
        self.valid_focused_color = color;
        self
    }

    /// Set the border color in focused case when input is invalid
    pub fn invalid_focused_color(mut self, color: Color) -> Self {
        self.invalid_focused_color = color;
        self
    }

    /// Set the border color in hovered case when input is valid
    pub fn valid_hovered_color(mut self, color: Color) -> Self {
        self.valid_hovered_color = color;
        self
    }

    /// Set the border color in hovered case when input is invalid
    pub fn invalid_hovered_color(mut self, color: Color) -> Self {
        self.invalid_hovered_color = color;
        self
    }

    fn validate(&mut self) -> State {
        if let Err(err) = self.value.validate() {
            self.error_message = err.base().unwrap()[0].message().to_string();
            State::Invalid
        } else {
            self.error_message = String::new();
            State::Valid
        }
    }
}

impl<'a, Message, V, T> Component<Message, Renderer> for TextInputValidated<'a, Message, V, T>
where
    V: Validator<T> + Clone,
    Message: Clone,
{
    type State = State;

    type Event = Event;

    fn update(&mut self, state: &mut Self::State, event: Self::Event) -> Option<Message> {
        match event {
            Event::OnChange(value) => {
                self.value.value_str(&value);
                *state = self.validate();
                if *state == State::Valid {
                    if let Some(on_valid_input) = self.on_valid_input.as_deref() {
                        Some(on_valid_input(self.value.to_value().unwrap()))
                    } else {
                        self.on_input.as_deref().map(|on_input| (on_input)(value))
                    }
                } else {
                    self.on_input.as_deref().map(|on_input| (on_input)(value))
                }
            }
            Event::OnSubmit => {
                *state = self.validate();
                if *state == State::Valid && self.on_valid_submit.is_some() {
                    self.on_valid_submit.clone()
                } else if self.on_submit.is_some() {
                    self.on_submit.clone()
                } else {
                    None
                }
            }
            Event::OnPaste(value) => {
                self.value.value_str(&value);
                *state = self.validate();
                if *state == State::Valid {
                    if let Some(on_valid_paste) = self.on_valid_paste.as_deref() {
                        Some(on_valid_paste(self.value.to_value().unwrap()))
                    } else {
                        self.on_paste
                            .as_deref()
                            .map(|on_paste: &dyn Fn(String) -> Message| (on_paste)(value))
                    }
                } else {
                    self.on_paste.as_deref().map(|on_paste| (on_paste)(value))
                }
            }
        }
    }

    fn view(&self, state: &Self::State) -> Element<Self::Event, Renderer> {
        let mut text_input = text_input(&self.placeholder, self.value.as_str())
            .width(self.width)
            .padding(self.padding)
            .line_height(self.line_height)
            .style(iced::theme::TextInput::Custom(Box::new(
                TextInputValidatedTheme::new(
                    state,
                    self.valid_active_color,
                    self.valid_focused_color,
                    self.valid_hovered_color,
                    self.invalid_active_color,
                    self.invalid_focused_color,
                    self.invalid_hovered_color,
                ),
            )));
        if self.on_input.is_some() || self.on_valid_input.is_some() {
            text_input = text_input.on_input(Event::OnChange);
        }
        if self.on_submit.is_some() || self.on_valid_submit.is_some() {
            text_input = text_input.on_submit(Event::OnSubmit);
        }
        if self.on_paste.is_some() || self.on_valid_paste.is_some() {
            text_input = text_input.on_paste(Event::OnPaste);
        }
        if let Some(id) = self.id.clone() {
            text_input = text_input.id(id);
        }
        if self.is_secure {
            text_input = text_input.password();
        }
        if let Some(font) = self.font {
            text_input = text_input.font(font);
        }
        if let Some(size) = self.size {
            text_input = text_input.size(size);
        }
        let handle = if *state == State::Valid {
            svg::Handle::from_path(format!(
                "{}/resources/check-symbol-4794.svg",
                env!("CARGO_MANIFEST_DIR")
            ))
        } else {
            svg::Handle::from_path(format!(
                "{}/resources/forbidden-10030.svg",
                env!("CARGO_MANIFEST_DIR")
            ))
        };
        let svg = svg(handle)
            .width(Length::Fixed(self.validator_image_size))
            .height(self.validator_image_size);
        row![
            tooltip(
                text_input,
                &self.error_message,
                tooltip::Position::FollowCursor,
            ),
            svg
        ]
        .into()
    }
}

struct TextInputValidatedTheme {
    validation_state: State,
    valid_active_color: Color,
    valid_focused_color: Color,
    valid_hovered_color: Color,
    invalid_active_color: Color,
    invalid_focused_color: Color,
    invalid_hovered_color: Color,
}

impl TextInputValidatedTheme {
    pub fn new(
        validation_state: &State,
        valid_active_color: Color,
        valid_focused_color: Color,
        valid_hovered_color: Color,
        invalid_active_color: Color,
        invalid_focused_color: Color,
        invalid_hovered_color: Color,
    ) -> Self {
        Self {
            validation_state: *validation_state,
            valid_active_color,
            valid_focused_color,
            invalid_active_color,
            invalid_focused_color,
            valid_hovered_color,
            invalid_hovered_color,
        }
    }
}

impl text_input::StyleSheet for TextInputValidatedTheme {
    type Style = Theme;

    fn active(&self, style: &Self::Style) -> text_input::Appearance {
        let palette = style.extended_palette();
        let border_color = match self.validation_state {
            State::Valid => self.valid_active_color,
            State::Invalid => self.invalid_active_color,
            State::Unchecked => palette.background.strong.color,
        };
        text_input::Appearance {
            background: palette.background.base.color.into(),
            border_radius: 2.0.into(),
            border_width: 1.0,
            border_color,
            icon_color: palette.background.weak.text,
        }
    }

    fn focused(&self, style: &Self::Style) -> text_input::Appearance {
        let palette = style.extended_palette();
        let border_color = match self.validation_state {
            State::Valid => self.valid_focused_color,
            State::Invalid => self.invalid_focused_color,
            State::Unchecked => palette.primary.strong.color,
        };

        text_input::Appearance {
            background: palette.background.base.color.into(),
            border_radius: 2.0.into(),
            border_width: 1.0,
            border_color,
            icon_color: palette.background.weak.text,
        }
    }

    fn placeholder_color(&self, style: &Self::Style) -> iced::Color {
        let palette = style.extended_palette();

        palette.background.strong.color
    }

    fn value_color(&self, style: &Self::Style) -> iced::Color {
        let palette = style.extended_palette();

        palette.background.base.text
    }

    fn disabled_color(&self, style: &Self::Style) -> iced::Color {
        self.placeholder_color(style)
    }

    fn selection_color(&self, style: &Self::Style) -> iced::Color {
        let palette = style.extended_palette();

        palette.primary.weak.color
    }

    fn disabled(&self, style: &Self::Style) -> text_input::Appearance {
        let palette = style.extended_palette();

        text_input::Appearance {
            background: palette.background.weak.color.into(),
            border_radius: 2.0.into(),
            border_width: 1.0,
            border_color: palette.background.strong.color,
            icon_color: palette.background.strong.color,
        }
    }

    fn hovered(&self, style: &Self::Style) -> text_input::Appearance {
        let palette = style.extended_palette();

        let border_color = match self.validation_state {
            State::Valid => self.valid_hovered_color,
            State::Invalid => self.invalid_hovered_color,
            State::Unchecked => palette.background.base.text,
        };
        text_input::Appearance {
            background: palette.background.base.color.into(),
            border_radius: 2.0.into(),
            border_width: 1.0,
            border_color,
            icon_color: palette.background.weak.text,
        }
    }
}

impl<'a, Message, V, T> From<TextInputValidated<'a, Message, V, T>>
    for Element<'a, Message, Renderer>
where
    Message: Clone + 'a,
    V: Validator<T> + Clone + 'a,
    T: 'a,
{
    fn from(value: TextInputValidated<'a, Message, V, T>) -> Self {
        component(value)
    }
}
