use validations::Validate;

/// Validator must be implemented for the value of [`TextInputValidated`]
///
/// [`TextInputValidated`]: crate::TextInputValidated
pub trait Validator<T>: Validate<()> + From<T> {
    /// Just takes over the string value to be check
    fn value_str(&mut self, value_str: &str);
    /// Returns the stored string value, this should also be invalid strings
    fn as_str(&self) -> &str;
    /// Returns the value coresponding to the string and `None` if not valid
    fn to_value(&self) -> Option<T>;
}
